<?php
if($user){
?>
    <table class="table table-striped table-bordered  table-condensed text-left">

        <tr>
            <td><strong>Category Name</strong></td>
            <td> {{ $user->category_name }}</td>
        </tr>
        <td><strong>Category Image</strong></td>
            
        <?php
            if($url_check==1){
            $url = asset('category_images/'.$user->category_image);
                $src = $url;
             }else{
                $src = asset('images/placeholder_cover(200x150).png');
            }
                         
        ?>
<td> <img src="{{ $src }}" alt="Category image" class="img-responsive category-preview-img img-width"> 
</td>
        <tr>
            <td><strong>Created</strong></td>
            <td>{{ date('d-m-Y', strtotime($user->created_at)) }}</td>
        </tr>
    </table>
    
<?php
}
?>    
    
