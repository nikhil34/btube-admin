<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Registration_model extends Model
{
     protected $table = 'users';

     protected $fillable = ['name','email','first_name','last_name','phone'];

}
