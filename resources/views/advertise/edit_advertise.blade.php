<?php
$title = array(
  'id'  => 'e_title',
  'maxlength' => 80,
  'class' => 'form-control'
);

$position = array(
  'id'  => 'e_position',
  'class' => 'form-control',
);

$iframe_code = array(
  'id'  => 'e_iframe_code',
  'class' => 'form-control',
);

$url = array(
  'id'  => 'e_image_url',
  'class' => 'form-control',
);
?>

      <form role="form" id="edit_advertise_form" enctype="multipart/form-data" data-advertise_id="{{ $user->advertise_id }}" name="edit_advertise_form">
        <div class="modal-body">
          <div class="card-content">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group label-floating">
                  <label class="control-label">Title</label>
                  <?php echo Form::text('title', $user->title,  array('class'=>'form-control', 'id'=>'e_title')); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-3 label-on-left">Select Any Options</label>
              <div class="col-sm-4 checkbox-radios">
                  <div class="radio">
                      <label style="font-weight: bold;">
                          <input <?php if($user->iframe_code != ""){ echo "checked"; } ?> id="e_adv_url" name="e_adv_opt" value="adv_url" type="radio"><span class="circle"></span><span class="check"></span> URL
                      </label>
                  </div>
              </div>
              <div class="col-sm-4 checkbox-radios">
                  <div class="radio">
                      <label style="font-weight: bold;">
                          <input <?php if($user->image != ""){ echo "checked"; } ?> id="e_adv_image" name="e_adv_opt" value="adv_image" type="radio"><span class="circle"></span><span class="check"></span> Image
                      </label>
                  </div>
              </div>
            </div>
             <!-- select iframe URL   -->
            <div id="e_select_adv_url"  style="display: none;">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating">
                    <label class="control-label">Iframe URL</label>
                    <?php echo Form::textarea('iframe_url', $user->iframe_code, array('class'=>'form-control', 'rows'=>5, 'id'=>'e_iframe_url')); ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- select  image   --> 
            <div id="e_select_adv_image" style="display: none;">
              <div class="row">
                <div class="col-lg-6">
                <?php
                  if($url_check==1){
                  $url = asset('advertise_images/'.$user->image);
                      $src = $url;
                   }else{
                      $src = asset('images/placeholder_cover(200x150).png');
                  }            
                ?>
                  <div id="upload_file_coin_type_edit" style="float: left;">
                    <div class="fileinput-new thumbnail pic-thumb">
                      <img src="{{ $src }}" alt="" id="placeholder_image">
                      <input type="hidden" value="<?php echo $user->image; ?>" name="advertise_image" id="e_advertige_image" />
                    </div>        
                  </div>
                  <a href="#" class="btn btn-primary btn-sm" id="upload_image_edit" style="margin-left: 15px;" ><span class="fa fa-plus " style="margin-right: 5px;"></span>Upload</a>
                  <img src="{{asset('images/loading.gif') }}" id="loader_image" style="margin-top:4px;margin-left:10px; display: none;">
                  <div class="clearfix"></div>
                  <div id="upload_file_res"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating">
                    <label class="control-label">Image Url</label>
                    <?php echo Form::text('image_url', $user->image_url, array('class'=>'form-control', 'id'=>'e_image_url')); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="btn-group">
                    <select class="" name="position" id="e_position" data-size="7" data-style="select-with-transition" title="Select Position" tabindex="-98">
                      <option disabled="">Select Position</option>
                      <option <?php if($user->position == 1){ echo 'selected'; } ?> value="1">Sidebar </option>
                      <option <?php if($user->position == 2){ echo 'selected'; } ?> value="2">Header</option>
                      <option <?php if($user->position == 3){ echo 'selected'; } ?> value="3">Content</option>
                      <option <?php if($user->position == 4){ echo 'selected'; } ?> value="4">Footer</option>
                    </select>
                </div>
              </div>
            </div>
          </div> <!-- card content -->
        </div>
        <div class="modal-footer">
          <div id="edit_res" style="margin-top:5px;"></div>
          <button type="submit" class="btn btn-primary pull-right" style="margin-left: 5px;">Save</button>
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-left: 5px;">Back</button>
          <img src="{{ asset('assets/images/loading.gif')}}"  id="edit-loader" class="pull-right" style="display: none;  "/>
          <div class="clearfix"></div>  
        </div>
      </form> 
<form name="attachment_edit" id="attachment_edit" enctype = 'multipart/form-data' action="{{ url('advertise/upload')}}" method="post" accept-charset="utf-8">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="file" name="image_file" value="" id="image_file_2" style="display: none;">
<input  type="hidden" name="path" value="category_images"/> 
</form>
<script>

$(document).ready(function(e){
  var placeholder_image = "{{asset('images/placeholder_cover(200x150).png') }}";
var e_iframe_code = "<?php echo $user->iframe_code; ?>"
var e_image = "<?php echo $user->image; ?>"

  if(e_iframe_code){
    $('#e_select_adv_url').removeAttr('style')
  }
  if(e_image){
    $('#e_select_adv_image').removeAttr('style')
  }

   $('#e_adv_image').click(function(){
      $('#e_select_adv_image').removeAttr('style');
      $('#e_select_adv_url').css('display','none');
  });
  $('#e_adv_url').click(function(){
    $('#e_select_adv_url').removeAttr('style');
    $('#e_select_adv_image').css('display','none');
  });

    $('#edit_advertise_form').submit(function(e){
        e.preventDefault();
            
        var title = $('#e_title').val();
        var position = $('#e_position').val();
        if($('input[name=e_adv_opt]:checked').val() == 'adv_url'){
            var iframe_code = $('#e_iframe_url').val();
             var image_url = '';
              var category_image = '';
        }else{
             var image_url = $('#e_image_url').val();
             var category_image = $('#e_advertige_image').val();
             var iframe_code = '';
        }
        var values = 'title='+title+'&position='+position+'&iframe_code='+iframe_code+'&image_url='+image_url+'&advertise_image='+category_image;
         id = $(this).attr('data-advertise_id');
        $.ajax({
            type:'post',
            url:"{!! url('advertise/update') !!}/"+id,
            dataType: 'json',
            data:values,
           success:function(result)
           {
                if(result.status==1)
            {
                $('#edit_advertise_form')[0].reset();
                $('#upload_file_coin_type_edit').empty();
                $('#upload_file_coin_type_edit').append('<div class="fileinput-new thumbnail pic-thumb"><img src="'+placeholder_image+'" alt=""></div>');
                 $('#edit_modal').modal('toggle');
                  $('#alert_area_add').empty().html('<div class="alert alert-success  alert-dismissable" id="disappear"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
                setTimeout(function(){$('#disappear').fadeOut('slow')},3000);
                emp_datatables.fnDraw();
            }
            else
            {
                $('#alert_area_add').empty().html('<div class="alert alert-danger  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
            }
           } 
        });
    });


      //upload image code 
 $('#upload_image_edit').click(function(e){
       e.preventDefault();
       $('#image_file_2').trigger('click'); 
    });
    
    $('#image_file_2').change(function(){
         $("#attachment_edit").submit();
     });
     
    var options = { 
    beforeSend: function() 
    {
        $("#loader_image").show();
        
    },
    complete: function(response) 
    {

           $("#loader_image").hide();
           var result = jQuery.parseJSON(response.responseText);
           if(result.status==0)
           {
               $('#add_coin_type_response').empty().html('<div class="alert alert-danger  alert-dismissible"  style="margin-top:5px;" ><a style="margin-top:3px;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+result.message+'</div>');
           }
           else if(result.status==1)
           {
               $('#upload_file_coin_type_edit').empty();
               $('#add_coin_type_response').empty();
               $('#upload_file_coin_type_edit').append('<div class="fileinput-new thumbnail pic-thumb" style="width: 210px; height: 160px; padding:5px"><a href="#" id="remove_image_edit" class="remove_image" title="Remove">&times;</a><img src="'+result.image_path+'"style="width: 200px; height: 150px; alt=""><input type="hidden" value="'+result.filename+'" name="category_image" id="e_advertige_image" /></div>');
               
           }
    },
    error: function()
    {
        alert(" ERROR: unable to upload files");
 
    }
 
  }; 
  $("#attachment_edit").ajaxForm(options);


    
});//ready

$(document).on('click','#remove_image_edit',function(e){
       e.preventDefault();
       $('#image_file_2').val('');
       $('#upload_file_coin_type_edit').empty();
       $('#upload_file_coin_type_edit').append('<div class="fileinput-new thumbnail pic-thumb" ><img src="{{ asset('images/placeholder_cover(200x150).png') }}" alt=""></div>');
});

</script>  
