<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Advertise_model extends Model
{
     protected $table = 'advertise';

     protected $fillable = ['title','iframe_code','image','image_url','position'];


}
