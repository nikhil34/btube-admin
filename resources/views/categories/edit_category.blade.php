<?php
$category_name = array(
  'id'  => 'e_category_name',
  'maxlength' => 80,
  'class' => 'form-control'
);

$description = array(
  'id'  => 'e_description',
  'class' => 'form-control',
  'rows' => '3'
);


?>

      <form role="form" id="edit_employee_form" data-employee_id="{{ $user->id }}" name="edit_employee_form">
        <div class="modal-body" style="overflow: auto;">
          


          <div class="card-content">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group label-floating">
                  <label class="control-label">Category Name</label>
                  <?php echo Form::text('category_name', $user->category_name, array('class'=>'form-control', 'id'=>'e_category_name')); ?>
                </div>
              </div>
            </div>
            <div class="row">
           <div class="col-lg-6">
                <?php
            if($url_check==1){
            $url = asset('category_images/'.$user->category_image);
                $src = $url;
             }else{
                $src = asset('images/placeholder_cover(200x150).png');
            }
                         
        ?>       
                       <div id="upload_file_coin_type_edit" style="float: left;">
                       <div class="fileinput-new thumbnail pic-thumb">
                     <img src="{{ $src }}" alt="" id="placeholder_image">
                     <input type="hidden" value="<?php echo $user->category_image; ?>" name="category_image" />
                       </div> 
                       
                      </div>
                        <a href="#" class="btn btn-primary btn-sm" id="upload_image_edit" style="margin-left: 15px;" ><span class="fa fa-plus " style="margin-right: 5px;"></span>Upload</a>
                       <img src="{{asset('images/loading.gif') }}" id="loader_image" style="margin-top:4px;margin-left:10px; display: none;">
                       <div class="clearfix"></div>
                       <div id="upload_file_res"></div>
                       </div>
            </div>
          </div>  
                        
                       
         </div>
      <div class="modal-footer">
      
      <div class="clearfix"></div>
        <div id="edit_res" style="margin-top:5px;"></div>
        <button type="submit" class="btn btn-primary pull-right" style="margin-left: 5px;">Save</button>
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal" style="margin-left: 5px;">Back</button>
        <img src="{{ asset('assets/images/loading.gif')}}"  id="edit-loader" class="pull-right" style="display: none;  "/>
        <div class="clearfix"></div>
         
      </div>
</form> 
<form name="attachment_edit" id="attachment_edit" enctype = 'multipart/form-data' action="{{ url('categories/upload')}}" method="post" accept-charset="utf-8">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="file" name="image_file" value="" id="image_file_2" style="display: none;">
<input  type="hidden" name="path" value="category_images"/> 
</form>
<script>
$(document).ready(function(e){
var placeholder_image = "{{asset('images/placeholder_cover(200x150).png') }}";
    $('#edit_employee_form').submit(function(e){
        e.preventDefault();
        id = $(this).attr('data-employee_id');
        $('#edit-loader').fadeIn();
        var values = $("#edit_employee_form").serialize();
        $.ajax({
            url:"{{ url('categories/update') }}/"+id,
            type:'post',
            dataType: 'json',
            data:values,
            success:function(result){
                 $('#edit-loader').fadeOut(); 
                if(result.status==1)
                {   
                    $('#edit_employee_form')[0].reset();
                    $('#upload_file_coin_type_edit').empty();
                    $('#upload_file_coin_type_edit').append('<div class="fileinput-new thumbnail pic-thumb"><img src="'+placeholder_image+'" alt=""></div>');
                    $('#edit_modal').modal('toggle');
                    $('#alert_area_add').empty().html('<div class="alert alert-success  alert-dismissable" id="disappear"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
                    setTimeout(function(){$('#disappear').fadeOut('slow')},3000);
                    emp_datatables.fnDraw(); 
                }
                else
                {
                    $('#alert_area_add').empty().html('<div class="alert text-left alert-danger id="disappear_edit_alert"  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
                    //setTimeout(function(){$('#disappear_edit_alert').fadeOut('slow')},3000)
                }
            }
        });
    });


      //upload image code 
 $('#upload_image_edit').click(function(e){
       e.preventDefault();
       $('#image_file_2').trigger('click'); 
    });
    
    $('#image_file_2').change(function(){
         $("#attachment_edit").submit();
     });
     
    var options = { 
    beforeSend: function() 
    {
        $("#loader_image").show();
        
    },
    complete: function(response) 
    {

           $("#loader_image").hide();
           var result = jQuery.parseJSON(response.responseText);
           if(result.status==0)
           {
               $('#add_coin_type_response').empty().html('<div class="alert alert-danger  alert-dismissible"  style="margin-top:5px;" ><a style="margin-top:3px;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+result.message+'</div>');
           }
           else if(result.status==1)
           {
               $('#upload_file_coin_type_edit').empty();
               $('#add_coin_type_response').empty();
               $('#upload_file_coin_type_edit').append('<div class="fileinput-new thumbnail pic-thumb" style="width: 210px; height: 160px; padding:5px"><a href="#" id="remove_image_edit" class="remove_image" title="Remove">&times;</a><img src="'+result.image_path+'"style="width: 200px; height: 150px; alt=""><input type="hidden" value="'+result.filename+'" name="category_image" /></div>');
               
           }
    },
    error: function()
    {
        alert(" ERROR: unable to upload files");
 
    }
 
  }; 
  $("#attachment_edit").ajaxForm(options);


    
});//ready

$(document).on('click','#remove_image_edit',function(e){
       e.preventDefault();
       $('#image_file_2').val('');
       $('#upload_file_coin_type_edit').empty();
       $('#upload_file_coin_type_edit').append('<div class="fileinput-new thumbnail pic-thumb" ><img src="{{ asset('images/placeholder_cover(200x150).png') }}" alt=""></div>');
});

</script>  
