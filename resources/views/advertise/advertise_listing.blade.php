@extends('layouts.inner')
@section('title', "Advertise")
@section('index_title'){{ $index_title }} @endSection
@section('content')
<style>
table.dataTable tr.odd { background-color:  #F4F6F6; }
</style>
            <div class="content">
                <div class="container-fluid">
                  <div class="row">
                  <div id="alert_area_add"></div>
                      <div class="col-md-12">
                          <div class="card">
                              <div class="card-header card-header-icon" data-background-color="purple">
                                 <i class="material-icons">assignment</i>
                              </div>
                              
                           
                            <div class="card-content">
                              <div class="pull-right" >
                              <button type="button" class="btn btn-primary add_form_button" title="Add Advertise" data-toggle="modal" data-target="#addAdvertise"><i class="material-icons" style="margin-right: 5px;">add</i>Add<div class="ripple-container"></div></button>
                             </div>
                                <h4 class="card-title">Advertise</h4>
                                <div class="toolbar"></div>
                                  <table id='empTable' class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th>Title </th>
                                        <th>Created</th>
                                        <th>action</th>
                                      </tr>
                                    </thead>
                                  </table>
                              </div>
                            </div>
                          </div>
                    </div>
                </div>
              </div>



<!--Add Advertise Modal -->
<div class="modal fade" id="addAdvertise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form name="add_advertise_form" id="add_advertise_form" method="post" enctype="multipart/form-data" files="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Advertise</h4>
      </div>
      

        <div class="modal-body">
          <div class="card-content">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group label-floating">
                  <label class="control-label">Title</label>
                  <?php echo Form::text('title', null, array('class'=>'form-control', 'id'=>'title')); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <label class="col-sm-3 label-on-left">Select Any Options</label>
              <div class="col-sm-4 checkbox-radios">
                  <div class="radio">
                      <label style="font-weight: bold;">
                          <input checked="true" id="adv_url" name="adv_opt" value="adv_url" type="radio"><span class="circle"></span> URL
                      </label>
                  </div>
              </div>
              <div class="col-sm-4 checkbox-radios">
                  <div class="radio">
                      <label style="font-weight: bold;">
                          <input id="adv_image" name="adv_opt" value="adv_image" type="radio"><span class="circle"></span><span class="check"></span> Image
                      </label>
                  </div>
              </div>
            </div>
             <!-- select URL   -->
            <div id="select_adv_url">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating">
                    <label class="control-label">Iframe URL</label>
                    <?php echo Form::textarea('iframe_url', null, array('class'=>'form-control', 'rows'=>5, 'id'=>'iframe_url')); ?>
                  </div>
                </div>
              </div>
            </div>
            <!-- select image   --> 
            <div id="select_adv_image" style="display: none;">
              <div class="row">
                <div class="col-lg-6">
                       
                  <div id="upload_file_coin_type" style="float: left;">
                    <div class="fileinput-new thumbnail pic-thumb">
                      <img src="{{ asset('images/placeholder_cover(200x150).png') }}" alt="" id="placeholder_image">
                    </div>        
                  </div>
                  <a href="#" class="btn btn-primary btn-sm" id="upload_image" style="margin-left: 15px;" ><span class="fa fa-plus " style="margin-right: 5px;"></span>Upload</a>
                  <img src="{{asset('images/loading.gif') }}" id="loader_image" style="margin-top:4px;margin-left:10px; display: none;">
                  <div class="clearfix"></div>
                  <div id="upload_file_res"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group label-floating">
                    <label class="control-label">Image Url</label>
                    <?php echo Form::text('image_url', null, array('class'=>'form-control', 'id'=>'image_url')); ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="btn-group bootstrap-select">
                    <select class="selectpicker" name="position" id="position" data-size="7" data-style="select-with-transition" title="Select Position" tabindex="-98">
                      <option disabled="">Select Position</option>
                      <option value="1">Sidebar </option>
                      <option value="2">Header</option>
                      <option value="3">Content</option>
                      <option value="4">Footer</option>
                    </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
      
      
      
         <button type="submit" class="btn btn-primary pull-right" style="margin-left: 5px;">Save Changes</button>
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal"  style="margin-left: 5px;">Back</button>
         <div class="clearfix"></div>
         
      </div>
</form> 
<form name="attachment" id="attachment" enctype = 'multipart/form-data' action="{{ url('advertise/upload')}}" method="post" accept-charset="utf-8">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="file" name="image_file" value="" id="image_file_1" style="display: none;">
<input  type="hidden" name="path" value="advertise_images"/> 
</form>
  <div class="col-xs-12">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        @endif
    </div> 
    
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--View Modal -->
<div class="modal fade" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Advertise Details</h4>
      </div>
      <div class="modal-body">
       <div id="response_view"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Edit Modal -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Advertise Details</h4>
      </div>
       <div id="edit_response_view"></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript" src="{{asset('js/jquery.datatables.js') }}"></script>
<script type="text/javascript" src="{{asset('js/datatables.bootstrap.js')}}"></script>
<!-- jquery.form.js -->
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script>
    var emp_datatables = "";
    var placeholder_image = "{{asset('images/placeholder_cover(200x150).png') }}";
    $(document).ready(function(){


      $('#adv_image').click(function(){
          $('#select_adv_image').removeAttr('style');
          $('#select_adv_url').css('display','none');

      });
      $('#adv_url').click(function(){
          $('#select_adv_url').removeAttr('style')
          $('#select_adv_image').css('display','none');
      });


         emp_datatables = $('table#empTable').dataTable({
         processing: true,
         serverSide: true,
         ajax:{
            "dataType": 'json',
            "type": "POST",
            "url": '{!! url('advertise/list') !!}'
         } ,

            "createdRow": function ( row, data, index ) {
            var links="";
            links += '<a href="#" data-advertise_id="'+data[2]+'" title="View Advertise Details" class="btn btn-info view_advertise" style="margin-right:5px;" ><i class="material-icons">search</i></a>';  
            links += '<a href="#" data-advertise_id="'+data[2]+'" title="Edit Details" class="btn btn-success edit_advertise" style="margin-right:5px;" ><i class="material-icons">edit</i></a>';  
            links +='<a href="#" data-advertise_id="'+data[2]+'" title="Delete Advertise"class="btn btn-danger delete_advertise" style="margin-right:5px;"><i class="material-icons">delete</i></a>';  
           

            var dateSplit = data[1].split("-");            
            day = dateSplit[2].split(' ');
            var curr_date = day[0];
            var curr_month = dateSplit[1]; //Months are zero based
            var curr_year = dateSplit[0];
            
            $('td:eq(2)', row).html( links);
            $('td:eq(1)', row).html(curr_month + "-" + curr_date + "-" + curr_year );
        },
        aoColumnDefs: [
       
          {
             bSortable: false,
             aTargets: [2]
          },
        
          
          {
             bSearchable: false,
             aTargets: [2]
          }
        ],
        });

        //Add Employee
      $('#add_advertise_form').submit(function(e){
        e.preventDefault();
            
       // var values = $("#add_advertise_form").serialize();
        var title = $('#title').val();
        var position = $('#position').val();
        if($('input[name=adv_opt]:checked').val() == 'adv_url'){
            var iframe_code = $('#iframe_url').val();
             var image_url = '';
              var category_image = '';
        }else{
             var image_url = $('#image_url').val();
             var category_image = $('#advertige_image').val();
             var iframe_code = '';
        }
        var values = 'title='+title+'&position='+position+'&iframe_code='+iframe_code+'&image_url='+image_url+'&advertise_image='+category_image;

        $.ajax({
            type:'post',
            url:"{!! url('advertise/add') !!}",
            dataType: 'json',
            data:values,
           success:function(result)
           {
                if(result.status==1)
            {
                $('#add_advertise_form')[0].reset();
                $('#upload_file_coin_type').empty();
                $('#upload_file_coin_type').append('<div class="fileinput-new thumbnail pic-thumb"><img src="'+placeholder_image+'" alt=""></div>');
                 $('#addAdvertise').modal('toggle');
                  $('#alert_area_add').empty().html('<div class="alert alert-success  alert-dismissable" id="disappear"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
                setTimeout(function(){$('#disappear').fadeOut('slow')},3000);
                emp_datatables.fnDraw();
            }
            else
            {
                $('#alert_area_add').empty().html('<div class="alert alert-danger  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
            }
           } 
        });
     }); 


      //View employee
 $(document).on('click','.view_advertise',function(e){
    e.preventDefault();
    var id = $(this).attr('data-advertise_id');
    $.ajax({
       url:"{!! url('advertise/view') !!}/"+id,
       dataType: 'html',
       success:function(result)
       {
        $('#response_view').html(result);
       } 
    });
    $('#view_modal').modal('show');
 });

//Edit employee
 $(document).on('click','.edit_advertise',function(e){
    e.preventDefault();
    var id = $(this).attr('data-advertise_id');
    $.ajax({
       url:"{!! url('advertise/edit') !!}/"+id,
       dataType: 'html',
       success:function(result)
       {
        $('#edit_response_view').html(result);
       } 
    });
    $('#edit_modal').modal('show');
 });


 //delete employee
$(document).on('click','.delete_advertise',function(e){
    
    e.preventDefault();
    var response = confirm('Are you sure want to delete this advertise?');
    if(response){
     var id = $(this).attr('data-advertise_id');
                 $.ajax({
                         url:"{{ url('advertise/delete')}}/"+id,
                         type: 'POST',
                         dataType: 'json',
                            error:function(){
                                alert("Unable to process the request");
                            },
                         success:function(result){
                               if(result.status=="1")
                               {  
                                $('#alert_area_add').html('<div class="alert alert-success" id="disappear_del"  >'+result.message+'</div>'); 
                                setTimeout(function(){$('#disappear_del').fadeOut('slow')},3000)
                                emp_datatables.fnDraw();
                               }
                               else
                               { 
                                 $('#alert_area_add').html('<div class="alert alert-danger" id="disappear_del"  ><button class="close" data-dismiss="alert">&times;</button>'+result.message+'</div>'); 
                                
                               }
                           }
                         });
       return false;
     }
  });   


   //upload image code 
 $('#upload_image').click(function(e){
       e.preventDefault();
       $('#image_file_1').trigger('click'); 
    });
    
    $('#image_file_1').change(function(){
         $("#attachment").submit();
     });
     
    var options = { 
    beforeSend: function() 
    {
        $("#loader_image").show();
        
    },
    complete: function(response) 
    {

           $("#loader_image").hide();
           var result = jQuery.parseJSON(response.responseText);
           if(result.status==0)
           {
               $('#add_coin_type_response').empty().html('<div class="alert alert-danger  alert-dismissible"  style="margin-top:5px;" ><a style="margin-top:3px;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+result.message+'</div>');
           }
           else if(result.status==1)
           {
               $('#upload_file_coin_type').empty();
               $('#add_coin_type_response').empty();
               $('#upload_file_coin_type').append('<div class="fileinput-new thumbnail pic-thumb" style="width: 210px; height: 160px; padding:5px"><a href="#" id="remove_image" class="remove_image" title="Remove">&times;</a><img src="'+result.image_path+'"style="width: 200px; height: 150px; alt=""><input type="hidden" value="'+result.filename+'" name="category_image" id="advertige_image" /></div>');
               
           }
    },
    error: function()
    {
        alert(" ERROR: unable to upload files");
 
    }
 
  }; 
  $("#attachment").ajaxForm(options);

 

    });
$(document).on('click','#remove_image',function(e){
       e.preventDefault();
       $('#image_file_1').val('');
       $('#upload_file_coin_type').empty();
       $('#upload_file_coin_type').append('<div class="fileinput-new thumbnail pic-thumb" ><img src="{{ asset('images/placeholder_cover(200x150).png') }}" alt=""></div>');
});
 $.ajaxSetup({
    headers: { 'X-CSRF-Token' : '{!! csrf_token() !!}' }
    });
</script>

@endSection