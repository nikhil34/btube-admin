<?php

namespace App\Http\Controllers;

use Auth;
use View;
use DB;
use Datatables;
use Validator;
use Image;
use App\Http\Helpers\Common_helper;
use App\Models\Advertise_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Redirector;

class Advertise extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return redirect::to('auth/login');
        }
        $index_title = 'Advertise';
       
        return view('advertise/advertise_listing', ['index_title' => $index_title]);
    }

     public function list_advertise()
    {
         
        $advertise = Advertise_model::select(['title','created_at','advertise_id']);
        return Datatables::of($advertise)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_advertise(Request $request)
    {
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $data = Input ::all();

        $rules = array(
            'title' => 'required|max:255|unique:advertise,title',
            'position' => 'required|max:255'    
        );

        if($data['iframe_code']=='' && $data['image_url'] =="" &&  $data['advertise_image']==''){
            $rules['iframe_code'] = 'required|max:255'; 
        }

        $validator = Validator::make($data, $rules);
        $errors = $validator->getMessageBag()->toArray();
        if($validator->fails()){
            $msg ='';
            foreach($errors as $key=>$value){
               foreach($value as $k=>$v){
                  $msg .=$v.'<br/>'; 
                }
            }  
            return response()->json(array('status'=>0, 'message'=>$msg));  
        }

        $add_advertise = Advertise_model::create([
            'iframe_code' => $data['iframe_code'],
            'image' => $data['advertise_image'],
            'title' => $data['title'],
            'image_url' => $data['image_url'],
            'position' => $data['position']
         ]);
        if($add_advertise){
            $result["status"] = 1;
            $result["message"] = trans('auth.advertise_created');
        }else
        {
            $result["status"] = 0;
            $result["message"] =trans('auth.error_occured');
        }
        echo json_encode($result);
    }

    public function get_details(Request $request, $id)
    {  
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $user = DB::table('advertise')->select('advertise.*')
                                   ->where('advertise.advertise_id',$id)
                                   ->get();
        $url = asset('advertise_images/'.$user[0]->image);   
        $url_check = Common_helper::URL_exists($url);  

        return view('advertise.view_advertise', ['user' => $user[0], 'url_check' => $url_check]);
       
    }

    public function delete(Request $request, $id){
        if(!$request->ajax()){
            return trans('auth.not_ajax');
        }

        $deleted = Advertise_model::where('advertise_id',$id)->delete();
               
        return  response()->json(array('status'=>1,'message'=>trans('auth.advertise_deleted')));
        
    }

    public function edit_view(Request $request, $id)
    {  
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $user = DB::table('advertise')->select('advertise.*')
                                   ->where('advertise_id',$id)
                                   ->get();
        $url = asset('advertise_images/'.$user[0]->image);   
        $url_check = Common_helper::URL_exists($url);  

        return view('advertise.edit_advertise', ['user' => $user[0], 'url_check' => $url_check]);
       
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $data = Input ::all();

        $rules = array(
            'title' => 'required|max:255|unique:advertise,title,'.$id.',advertise_id',
            'position' => 'required|max:255'    
        );

        if($data['iframe_code']=='' && $data['image_url'] =="" &&  $data['advertise_image']==''){
            $rules['iframe_code'] = 'required|max:255'; 
        }

        $validator = Validator::make($data, $rules);
        $errors = $validator->getMessageBag()->toArray();
        if($validator->fails()){
            $msg ='';
            foreach($errors as $key=>$value){
               foreach($value as $k=>$v){
                  $msg .=$v.'<br/>'; 
                }
            }  
            return response()->json(array('status'=>0, 'message'=>$msg));  
        }

        $update_data = array('iframe_code' => $data['iframe_code'],
                        'image' => $data['advertise_image'],
                        'title' => $data['title'],
                        'image_url' => $data['image_url'],
                        'position' => $data['position']
                        );

        $update = Advertise_model::where('advertise.advertise_id', $id)
                ->update($update_data);

        if($update){
            $result["status"] = 1;
            $result["message"] = trans('auth.advertise_updated');
        }else
        {
            $result["status"] = 0;
            $result["message"] =trans('auth.error_occured');
        }
        echo json_encode($result);
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'image_file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',       
        ]);
        $image = $request->file('image_file');

        $input['category_image'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/advertise_images');

        $image->move($destinationPath, $input['category_image']);
        /*$add_image = categories_model::create([
            'category_image' => $input['category_image']
        ]);*/ 

        $data["status"] = 1;
        $data["filename"] =  $input['category_image'];
        //$data["original"] = $path;
        $data["image_path"]=asset('advertise_images/'.$input['category_image']);
        //unlink($output_dir.$file_name.$ext);
        echo json_encode($data);
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
