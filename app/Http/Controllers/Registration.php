<?php

namespace App\Http\Controllers;

use Auth;
use View;
use DB;
use Datatables;
use Validator;
use Image;
use App\Http\helpers\Common_helper;
use App\models\registration_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Redirector;

class Registration extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return redirect::to('auth/login');
        }
        $index_title = 'Registration';
       
        return view('registration/registration_list', ['index_title' => $index_title]);
    }

     public function registration_list()
    {
         
    
        $registration = registration_model::select(['first_name','email','phone','created_at','id','last_name']);
        return Datatables::of($registration)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee(Request $request)
    {
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $data = Input ::all();

        $rules = array(
            'category_name' => 'required|max:255|unique:categories,category_name,'    
        );

        $validator = Validator::make($data, $rules);
        $errors = $validator->getMessageBag()->toArray();
        if($validator->fails()){
            $msg ='';
            foreach($errors as $key=>$value){
               foreach($value as $k=>$v){
                  $msg .=$v.'<br/>'; 
                }
            }  
            return response()->json(array('status'=>0, 'message'=>$msg));  
        }

        $add_employee = categories_model::create([
            'category_name' => $data['category_name'],
            'category_image' => $data['category_image']
        ]); 
        if($add_employee){
            $result["status"] = 1;
            $result["message"] = trans('auth.category_created');
        }else
        {
            $result["status"] = 0;
            $result["message"] =trans('auth.error_occured');
        }
        echo json_encode($result);
    }

    public function get_details(Request $request, $id)
    {  
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $user = DB::table('users')->select('users.*')
                                   ->where('users.id',$id)
                                   ->get();
        echo Common_helper::URL_exists(2);                          
        //return view('registration.view_category', ['user' => $user[0]]);
       
    }

    public function delete(Request $request, $id){
        if(!$request->ajax()){
            return trans('auth.not_ajax');
        }

        $deleted = categories_model::where('id',$id)->delete();
               
        return  response()->json(array('status'=>1,'message'=>trans('auth.category_deleted')));
        
    }

    public function edit_register(Request $request, $id)
    {  
       if(!Auth::check()){
            return redirect::to('auth/login');
        }
        
        $user = DB::table('users')->select('users.*')
                                   ->where('users.id',$id)
                                   ->get();

        $url = asset('cover_images/'.$user[0]->coverPic);   
        $url_check = Common_helper::URL_exists($url);  

        $url_profile = asset('profile_images/'.$user[0]->profileImg);   
        $url_check_pro = Common_helper::URL_exists($url_profile);  

        return view('registration.edit-registration', ['user' => $user[0], 'url_check' => $url_check, 'url_profile' => $url_check_pro ]);
       
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        } 
        
        $data = Input ::all();
        
        $rules = array(
            'coverPic' => 'required|max:255',
            'profilePic' => 'required| max:255'   
        );
            
        $validator = Validator::make($data, $rules);
        $errors = $validator->getMessageBag()->toArray();
        if($validator->fails()){
            $msg ='';
            foreach($errors as $key=>$value){
               foreach($value as $k=>$v){
                  $msg .=$v.'<br/>'; 
                }
            }  
            return response()->json(array('status'=>0, 'message'=>$msg));  
        }

        $update_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'coverPic' => $data['coverPic'],
            'profileImg' => $data['profilePic'],
            'bioDescription' => $data['description']
        ); 

         $update = registration_model::where('id', $id)
            ->update($update_data);

        if($update){
            $result["status"] = 1;
            $result["message"] = trans('auth.users_updated');
        }else
        {
            $result["status"] = 0;
            $result["message"] =trans('auth.error_occured');
        }
        echo json_encode($result);
    }

     public function upload_cover_image(Request $request)
    {
        $this->validate($request, [
            'image_file_cover' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',       
        ]);
        $image = $request->file('image_file_cover');

        $input['coverPic'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/cover_images');

        $image->move($destinationPath, $input['coverPic']);
        /*$add_image = categories_model::create([
            'category_image' => $input['category_image']
        ]);*/ 

        $data["status"] = 1;
        $data["filename"] =  $input['coverPic'];
        //$data["original"] = $path;
        $data["image_path"]=asset('cover_images/'.$input['coverPic']);
        //unlink($output_dir.$file_name.$ext);
        echo json_encode($data);
      
    }

    public function upload_profile_image(Request $request)
    {
        $this->validate($request, [
            'image_file_pro' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',       
        ]);
        $image = $request->file('image_file_pro');

        $input['profileImg'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/profile_images');

        $image->move($destinationPath, $input['profileImg']);
        /*$add_image = categories_model::create([
            'category_image' => $input['category_image']
        ]);*/ 

        $data["status"] = 1;
        $data["filename"] =  $input['profileImg'];
        //$data["original"] = $path;
        $data["image_path"]=asset('profile_images/'.$input['profileImg']);
        //unlink($output_dir.$file_name.$ext);
        echo json_encode($data);
      
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
