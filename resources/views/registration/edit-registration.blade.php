@extends('layouts.inner')
@section('title', "Edit Registration")

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
        <div id="alert_area_add"></div>
            <div class="card">
                <div class="card-header card-header-text" data-background-color="purple">
                    <h4 class="card-title">Edit Registration</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <form method="post" id="edit_registration_form" data-user_id="{{$user->id}}" action="{{url('registration/Update')}}/{{$user->id}} " class="form-horizontal"  >
                        {!! csrf_field() !!}
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-sm-2 label-on-left">First Name</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label"></label>
                                        <input type="text" name="first_name" class="form-control" value="{{$user->first_name}}">
                                        <span class="help-block">A block of help text that breaks onto a new line.</span>
                                    <span class="material-input"></span></div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 label-on-left">Last Name</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label"></label>
                                        <input type="text" name="last_name" class="form-control" value="{{$user->last_name}}">
                                    <span class="material-input"></span></div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 label-on-left">Email</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <p class="form-control-static">{{$user->email}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 label-on-left">Created At</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <p class="form-control-static">{{$user->created_at}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 label-on-left">Phone Number</label>
                                <div class="col-sm-10">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label"></label>
                                        <input type="text" name="phone" class="form-control" value="{{$user->phone}}">
                                    <span class="material-input"></span></div>
                                </div>
                            </div>
                        </div>

                        <?php
                          if($url_check==1){
                          $url = asset('cover_images/'.$user->coverPic);
                              $src = $url;
                           }else{
                              $src = asset('img/image_placeholder.jpg');
                          }             
                        ?>
                        <div class="col-md-6">
                          <!-- cover images -->
                            <div class="row">
                                <label class="col-sm-2 label-on-left">Cover Image</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new text-center" id="upload_file_cover_type" style="float: left;">
                                        <div class="fileinput-new thumbnail edit-reg-img">
                                          <?php if($url_check==1){ ?>
                                          <a href="#" id="remove_cover_image" class="remove_image" title="Remove">&times;</a>
                                          <?php } ?>
                                            <img src="<?php echo $src; ?>" style="width: 380px; height: 170px; alt="" id="placeholder_image">
                                             <input type="hidden" value="<?php echo $user->coverPic;?>" name="coverPic" id="coverPic" />
                                        </div>
                                    </div>
                                    <a href="#" class="btn btn-primary btn-sm" id="upload_cover_image" style="margin-left: 15px;" ><span class="fa fa-plus " style="margin-right: 5px;"></span>Upload</a>
                                    <div class="clearfix"></div>
                                   <img src="{{asset('images/loading.gif') }}" id="loader_image_cover" class="loader_image_cover">
                                   <div class="clearfix"></div>
                                   <div id="upload_file_resc"></div>
                                </div>
                            </div>
                            <!-- cove images/ -->



                            <!-- profile images -->
                            <?php
                              if($url_profile==1){
                              $url_pro = asset('profile_images/'.$user->profileImg);
                                  $src_pro = $url_pro;
                               }else{
                                  $src_pro = asset('img/placeholder.jpg');
                              }             
                            ?>
                            <div class="row">
                                <label class="col-sm-2 label-on-left">Profile Image</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new text-center" id="upload_file_profile_type" style="float: left;">
                                        <div class="fileinput-new thumbnail img-circle">
                                        <?php if($url_profile==1){ ?>
                                          <a href="#" id="remove_profile_image" class="remove_profile_image" title="Remove">&times;</a>
                                          <?php } ?>
                                            <img src="<?php echo $src_pro; ?>" style="width: 200px; height: 110px; alt="" id="placeholder_image">
                                            <input type="hidden" value="<?php echo $user->profileImg;?>" name="profilePic" id="profilePic" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-10" style="float: right;">
                                    <a href="#" class="btn btn-primary btn-sm" id="upload_profile_image" style="margin-left: 15px;" ><span class="fa fa-plus " style="margin-right: 5px;"></span>Upload</a>
                                   <img src="{{asset('images/loading.gif') }}" id="loader_image_pro" class="loader_image_cover" >
                               </div>
                                   <div class="clearfix"></div>
                                   <div id="upload_file_res"></div>
                                
                            </div>
                            <!-- profile images/ -->
                        </div>
                        <div class="col-md-12">
                            <label class="col-sm-2">Description</label>
                            <div class="col-sm-10>
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label"></label>
                                    <textarea class="form-control" placeholder="Describe about yourself" name="description"> {{$user->bioDescription}} </textarea>
                                <span class="material-input"></span></div>
                          </div>
                        </div>
                        <input type="submit" class="btn btn-primary btn-sm" value="Update" style="float: right;" />
                        <a href="{{ url('registration')}}" class="btn btn-primary btn-sm" style="float: left;" >Back</a>
                    </form>
                </div><!-- row/ -->
            </div>  <!-- card-content/ -->
                
            </div> <!-- Card /-->
        </div>
    </div> <!-- container-fluid/ -->
</div>

<!-- cover image upload -->
<form name="attachment_cover" id="attachment_cover" enctype = 'multipart/form-data' action="{{ url('registration/cover_upload')}}" method="post" accept-charset="utf-8">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="file" name="image_file_cover" value="" id="image_file_cover" style="display: none;">
<input  type="hidden" name="path" value="user_cover_images"/> 
</form>

<!-- profile image upload  -->
<form name="attachment_profile" id="attachment_profile" enctype = 'multipart/form-data' action="{{ url('registration/profile_upload')}}" method="post" accept-charset="utf-8">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="file" name="image_file_pro" value="" id="image_file_pro" style="display: none;">
<input  type="hidden" name="path" value="user_cover_images"/> 
</form>


<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script>
var placeholder_cover = "{{asset('img/image_placeholder.jpg') }}";
    $(document).ready(function(){

      $('#edit_registration_form').submit(function(e){
        e.preventDefault();
        id = $(this).attr('data-user_id');
        //$('#edit-loader').fadeIn();
        var values = $("#edit_registration_form").serialize();
        $.ajax({
            url:"{{ url('registration/Update') }}/"+id,
            type:'post',
            dataType: 'json',
            data:values,
            success:function(result){
                 //$('#edit-loader').fadeOut(); 
                if(result.status==1)
                {   
                    $('#alert_area_add').empty().html('<div class="alert alert-success  alert-dismissable" id="disappear"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
                    setTimeout(function(){$('#disappear').fadeOut('slow')},3000);
                    emp_datatables.fnDraw(); 
                }
                else
                {
                    $('#alert_area_add').empty().html('<div class="alert text-left alert-danger id="disappear_edit_alert"  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
                    //setTimeout(function(){$('#disappear_edit_alert').fadeOut('slow')},3000)
                }
            }
        });
    });

        //upload cover image 
        $('#upload_cover_image').click(function(e){
           e.preventDefault();
           $('#image_file_cover').trigger('click'); 
        });
    
        $('#image_file_cover').change(function(){
             $("#attachment_cover").submit();
        });

         var options = { 
    beforeSend: function() 
    {
        $("#loader_image_cover").show();
        
    },
    complete: function(response) 
    {

           $("#loader_image_cover").hide();
           var result = jQuery.parseJSON(response.responseText);
           if(result.status==0)
           {
               $('#upload_file_resc').empty().html('<div class="alert alert-danger  alert-dismissible"  style="margin-top:5px;" ><a style="margin-top:3px;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+result.message+'</div>');
           }
           else if(result.status==1)
           {
               $('#upload_file_cover_type').empty();
               $('#upload_file_resc').empty();
               $('#upload_file_cover_type').append('<div class="fileinput-new thumbnail pic-thumb edit-reg-img"><a href="#" id="remove_cover_image" class="remove_image" title="Remove">&times;</a><img src="'+result.image_path+'"style="width: 380px; height: 170px; alt=""><input type="hidden" value="'+result.filename+'" name="coverPic" /></div>');
               
           }
    },
    error: function()
    {
        alert(" ERROR: unable to upload files");
 
    }
 
  }; 
  $("#attachment_cover").ajaxForm(options);


    $(document).on('click','#remove_cover_image',function(e){
       e.preventDefault();
       $('#image_file_cover').val('');
       $('#upload_file_cover_type').empty();
       $('#upload_file_cover_type').append('<div class="fileinput-new thumbnail edit-reg-img"><img src="{{ asset('img/image_placeholder.jpg') }}" alt=""></div>');
    });



    //upload profile image
        $('#upload_profile_image').click(function(e){
           e.preventDefault();
           $('#image_file_pro').trigger('click'); 
        });
    
        $('#image_file_pro').change(function(){
             $("#attachment_profile").submit();
        });

         var options = { 
    beforeSend: function() 
    {
        $("#loader_image_pro").show();
        
    },
    complete: function(response) 
    {

           $("#loader_image_pro").hide();
           var result = jQuery.parseJSON(response.responseText);
           if(result.status==0)
           {
               $('#upload_file_res').empty().html('<div class="alert alert-danger  alert-dismissible"  style="margin-top:5px;" ><a style="margin-top:3px;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+result.message+'</div>');
           }
           else if(result.status==1)
           {
               $('#upload_file_profile_type').empty();
               $('#upload_file_res').empty();
               $('#upload_file_profile_type').append('<div class="fileinput-new thumbnail pic-thumb img-circle"><a href="#" id="remove_profile_image" class="remove_profile_image" title="Remove">&times;</a><img src="'+result.image_path+'"style="width: 200px; height: 110px; alt=""><input type="hidden" value="'+result.filename+'" name="profilePic" /></div>');
               
           }
    },
    error: function()
    {
        alert(" ERROR: unable to upload files");
 
    }
 
  }; 
  $("#attachment_profile").ajaxForm(options);
});

    $(document).on('click','#remove_profile_image',function(e){
       e.preventDefault();
       $('#image_file_pro').val('');
       $('#upload_file_profile_type').empty();
       $('#upload_file_profile_type').append('<div class="fileinput-new thumbnail pic-thumb img-circle"><img src="{{ asset('img/placeholder.jpg') }}" alt=""></div>');
    });

    window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>


</script>
@endSection
            