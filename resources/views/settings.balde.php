@extends('layout.inner')
@section('title'){{ 'Settings' }}@endsection
@section('page-content')
<?php



$first_name = array(
	'name'	=> 'first_name',
	'id'	=> 'first_name',
	'maxlength'	=> 80,
	'size'	=> 30,
    'class'=>'form-control',
    'placeholder'=>'First Name',
    'data-parsley-trigger'=>"change",
    'data-parsley-required'=>"true",
);
$last_name = array(
	'name'	=> 'last_name',
	'id'	=> 'last_name',
	'maxlength'	=> 80,
	'size'	=> 30,
    'class'=>'form-control',
    'placeholder'=>'Last Name',
    'data-parsley-trigger'=>"change",
    'data-parsley-required'=>"true",
);


$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'maxlength'	=> 80,
	'size'	=> 30,
     'class'=>'form-control',
    'placeholder'=>'Email'
);
$new_email = array(
	'name'	=> 'new_email',
	'id'	=> 'new_email',
    'maxlength'	=> 80,
	'size'	=> 30,
     'class'=>'form-control',
    'placeholder'=>'New Email',
    'data-parsley-type'=>"email",
    'data-parsley-trigger'=>"change",
    'data-parsley-required'=>"true",
);
$vpassword = array(
	'name'	=> 'vpassword',
	'id'	=> 'vpassword',
	'maxlength'	=> 80,
	'class'=>'form-control',
    'placeholder'=>'Current Password',
    'data-parsley-trigger'=>"change",
    'data-parsley-required'=>"true",
);



$password = array(
	'name'	=> 'old_password',
	'id'	=> 'old_password',
	'maxlength'	=> 80,
	'class'=>'form-control',
    'placeholder'=>'Current Password',
    'data-parsley-trigger'=>"change",
    'data-parsley-required'=>"true",
);

$new_password = array(
	'name'	=> 'new_password',
	'id'	=> 'new_password',
	'maxlength'	=> 80,
	'class'=>'form-control',
    'placeholder'=>'New Password',
    'data-parsley-minlength'=>"6",
    'data-parsley-trigger'=>"change",
    'data-parsley-required'=>"true",
);

$confirm_new_password = array(
	'name'	=> 'new_password_confirmation',
	'id'	=> 'new_password_confirmation',
	'maxlength'	=> 80,
	'class'=>'form-control',
    'placeholder'=>'Confirm New Password',
    'data-parsley-trigger'=>"change",
    'data-parsley-required'=>"true",
    'data-parsley-equalto'=>"#new_password",
);



?> 
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          
          <h1>
            Settings
            <small>Change your basic and login settings</small>
          </h1>
         
        </section>

        
        <!-- Main content -->
        <section class="content">
                  <div class="row">
                   <div class="col-md-12 col-sm-12 col-xs-12">
                    <div id="alert_area">
                    @if(session('ok'))
                    <div class="alert alert-dismissable alert-success"><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>{{session('ok')}}</div>
                    @endif
                    </div>
                   </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                             <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Change Basic Information</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                           <form role="form" id="profile_edit_form" name="profile_edit_form" class="form-horizontal" method="post" action="{{ url('settings/profile')}}">
                            {!! Form::token() !!}
                           <div class="box-body">
                           <div class="form-group">
                    
                                <div class="col-lg-6">
                                    <label class="control-label" for="first_name">First Name</label>
                                    {!! Form::text('first_name', $user->first_name, $first_name) !!}
                                  
                                    <span style="color: red;">{{$errors->first('first_name')}}</span>
                                </div>
                                <div class="col-lg-6">
                                    <label class="control-label" for="last_name">Last Name</label>
                                    {!! Form::text('last_name', $user->last_name, $last_name) !!}
                                    <span style="color: red;">{{$errors->first('last_name')}}</span>
                                </div>
                                
                              </div> 
                          </div><!-- /.box-body -->
        
                          <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Save Changes</button>
                          </div>
                        </form>
                      </div>
                
                
                
                <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Change Email</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                          <form role="form" id="email_form" name="email_form" class="form-horizontal">
                          <div class="box-body">
                            <div class="form-group">
                                               <div class="col-lg-12">
                                                    <label class="control-label" for="email">Email</label>
                                                     {!! Form::text('email', $user->email, $email) !!}
                                               
                                                    <span style="color: red;"></span>
                                                </div>
                            </div> 
                          </div><!-- /.box-body -->
        
                          <div class="box-footer">
                             <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#changeEmail">Change Email</button>
                          </div>
                         </form>	
                      </div>
                
                
                
                <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Change Password</h3>
                        </div><!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" id="password_form" name="password_form" class="form-horizontal">
                           <div class="box-body">
                                                <div class="form-group">
                                                
                                                <div class="col-lg-12">
                                                    <label class="control-label" for="old_password">Current Password</label>
                                                   {!! Form::password('old_password',$password) !!}
                                                    <span style="color: red;"></span>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                
                                                <div class="col-lg-12">
                                                    <label class="control-label" for="new_password">New Password</label>
                                                    {!! Form::password('new_password',$new_password) !!}
                                                    <span style="color: red;"></span>
                                                </div>
                                              </div>
                                              <div class="form-group">
                                                <div class="col-lg-12">
                                                    <label class="control-label" for="new_password_confirmation">Confirm New Password</label>
                                                    {!! Form::password('new_password_confirmation',$confirm_new_password) !!}  </div>
                                                 </div>
                                              </div><!-- /.box-body -->
                         
                          <div class="box-footer">
                          <div class="pull-right">
                            <img src="{{ asset('assets/images/loading.gif') }}"  id="loader_pass"  style="display: none; margin-right: 5px;;"/>
      
                            <button type="submit" class="btn btn-primary">Change Password</button>
                          </div>
                          </div>
                      </form>	
                      </div>
              
              </div><!--./col-->
          </div><!--./row-->
     
        </section><!-- /.content -->
        
      </div><!-- /.content-wrapper -->
      
<!-- Change Email -->
<div class="modal fade" id="changeEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Enter password to change email</h4>
      </div>
      <div class="modal-body">
       <form role="form" id="new_email_form" name="new_email_form" class="form-horizontal">
                                     <div class="form-group">
                                        <div class="col-lg-12">
                                            <label class="control-label" for="vpassword">Password</label>
                                          {!! Form::password('vpassword',$vpassword) !!}
                                            <span style="color: red;"></span>
                                        </div>
                                      </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <label class="control-label" for="new_email">New Email</label>
                                           {!! Form::text('new_email', old('new_email'), $new_email) !!}
                                               
                                            <span style="color: red;"></span>
                                        </div>
                                      </div>
         <div class="clearfix"></div>
         <div id="new_email_form_res" ></div>
           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <img src="{{ asset('assets/images/loading.gif') }}"  id="new_email_form_loader"  style="display: none; margin-right: 5px;;"/>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>	
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
$('#changeEmail').on('show.bs.modal', function (e) {
  $('#new_email').val($('#email').val());
});

$(document).ready(function(){
   //attach parsley validation
  $('#profile_edit_form').parsley();
   //attach parsley validation
  $('#new_email_form').parsley();
  //change email
  $('#new_email_form').submit(function(){

            $('#new_email_form_loader').fadeIn();
            var values = $('#new_email_form').serialize();
            $.ajax({
            url:'{{url('settings/email')}}',
            dataType:'json',
            type:'POST',
            data:values,
            success:function(result){
               $('#new_email_form_loader').fadeOut();
             if(result.status==1)
             {
                $('#new_email_form_res').html('<div class="alert alert-success" id="disappear110" style="margin-top:5px;"  >'+result.message+'</div>');
                setTimeout(function(){$('#disappear110').fadeOut('slow')},3000)
                $('#new_email_form')[0].reset();
               
                
             }
             else
             {
                
                $('#new_email_form_res').empty().html('<div class="alert alert-danger"  style="margin-top:5px;" >'+result.message+'</div>');
                $('#vpassword').val('');
                
             } 
            }
           });
    
    return false;
    });
      
      //attach parsley validation
     $('#password_form').parsley();
     $('#password_form').submit(function(){

        var values = $('#password_form').serialize();
        $('#loader_pass').fadeIn();
        $.ajax({
            url:'{{ url('settings/password')}}',
            dataType:'json',
            type:'POST',
            data:values,
            success:function(result){
               $('#loader_pass').fadeOut();
             if(result.status==1)
             {
                
                $('#alert_area').html('<div class="alert alert-success" id="disappear_add"  >'+result.message+'</div>'); 
                setTimeout(function(){$('#disappear_add').fadeOut('slow')},3000)
               
             }
             else
             {
               $('#alert_area').empty().html('<div class="alert alert-danger" id="disappear_add">'+result.message+'</div>');
             }
             $('#password_form')[0].reset(); 
             $('html, body').animate({scrollTop: $('#disappear_add').offset().top-100}, 600);
              
            }
       });
   
       return false;
   });
    
});//ready
 
 //send csrf token in request header for ajax requests
$.ajaxSetup({
   headers: { 'X-CSRF-Token' : '{!! csrf_token() !!}' }
});
</script>
@endsection