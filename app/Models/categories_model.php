<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class categories_model extends Model
{
     protected $table = 'categories';

     protected $fillable = ['category_name','category_image','description'];

}
