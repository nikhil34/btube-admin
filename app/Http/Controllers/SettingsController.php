<?php

namespace App\Http\Controllers;
use Auth;
use View;
use Mail;
use DB;
use Hash;
use Validator;
use App\models\Emp_model;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Redirector;


class SettingsController extends Controller
{   
    /** constructor check for admin access*/
    public function __construct()
    {
     
    }
   
   /** get list view of staff
    */
    public function index(){
        $user = Emp_model::where('id',Auth::user()->id)
                            ->get();
        return view('settings',['user'=>$user[0]]);
    }
    
    /** save profile changes */
    public function changeProfile(){
         $data = Input::all();
      
         $rules = array(
            'first_name' => 'max:255|required',
            'last_name' => 'max:255|required',
         );
         
          // Create a new validator instance.
          $validator = Validator::make($data, $rules);
        
          $errors = $validator->getMessageBag()->toArray();
          
          if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($errors);
          }
          
        $profile = array(
           'first_name' => $data['first_name'],
           'last_name' =>$data['last_name'],
           );
        //update user
        $user = Emp_model::findOrFail(Auth::user()->id);

        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        
        $user->save();
        
        return Redirect::back()->with('ok',trans('auth.profile_updated'));   
          
    }
    
    public function changeEmail(Request $request){
       if(!$request->ajax()){
         return trans('auth.not_ajax');
       } 
         $data = Input::all();
          
         $rules = array(
            'new_email' => 'required|email|max:255',
            'vpassword' => 'required|max:255',
         );
    
          // Create a new validator instance.
          $validator = Validator::make($data, $rules);
        
          $errors = $validator->getMessageBag()->toArray();
          
          if($validator->fails()){
               $msg ='';
               foreach($errors as $key=>$value){
                   foreach($value as $k=>$v){
                      $msg .=$v.'<br/>'; 
                   }
               }
           
            return  response()->json(array('status'=>0,'message'=>$msg));
          }
          
         $user = DB::table('users')->select('users.*','first_name','last_name')
                                   ->where('users.id',Auth::user()->id)
                                   ->leftJoin('user_profiles','user_profiles.user_id', '=', 'users.id')
                                   ->get();
         if(count($user)){
            $user = $user[0];
            $emailData['user_id'] = $user->id;
           
            if(!Hash::check($data['vpassword'], $user->password)){
                $result = ['status'=>0,'message'=>trans('auth.incorrect_password')];
            }
            
            else if(strtolower($user->email) == strtolower($data['new_email'])){//current email
               $result = ['status'=>0,'message'=> trans('auth.current_email') ]; 
            }
            else if(strtolower($user->new_email) == strtolower($data['new_email'])){
                $emailData['new_email_key'] = $user->new_email_key;
                $result = ['status'=>1,'message'=>trans('email_change_sent')];
            }
            else{
                
                $row = DB::table('users')->select('email')
                                   ->where('email','=', strtolower($data['new_email']))
                                   ->get();
              
               if(count($row)){//email already exists
                 $result = ['status'=>0,'message'=>trans('auth.email_taken')];
               }
               else{
                 $user = User::findOrFail($user->id);
                 
                 $emailData['new_email_key'] = str_random(60);
                 
                 $user->new_email = strtolower($data['new_email']);
                 $user->new_email_key = $emailData['new_email_key'];
                 
                 $user->save();
                 
                 $result = ['status'=>1,'message'=>trans('auth.email_change_sent')];
                 
               }
            } 
            
            if($result['status'] == 1){
                $emailData['email'] = strtolower($data['new_email']);
                $emailData['type'] = 'change_email';
                $this->sendMail($emailData);
            }  
         }
         else{
            $result = ['status'=>0,'message'=>trans('auth.user_not_found')];
         }
          
            
            return response()->json($result);   
       
    }
    
     public function changePassword(Request $request){
       if(!$request->ajax()){
         return trans('auth.not_ajax');
       } 
         $data = Input::all();
          
         $rules = array(
            'old_password' => 'required|max:255',
            'new_password' => 'required|confirmed|min:6',
         );
    
          // Create a new validator instance.
          $validator = Validator::make($data, $rules);
        
          $errors = $validator->getMessageBag()->toArray();
          
          if($validator->fails()){
               $msg ='';
               foreach($errors as $key=>$value){
                   foreach($value as $k=>$v){
                      $msg .=$v.'<br/>'; 
                   }
               }
           
            return  response()->json(array('status'=>0,'message'=>$msg));
          }
          
          try{
                     $user = User::findOrFail(Auth::user()->id);
                     
                     if(!Hash::check($data['old_password'], $user->password)){//incorrect password
                       $result = ['status'=>0,'message'=>trans('auth.incorrect_password')];
                     }
                     else{
                        $user->password = bcrypt($data['new_password']);
                 
                        $user->save();
                        
                        $result = ['status'=>1,'message'=>trans('auth.password_changed')]; 
                     }
                    
           }
          catch(Exception $e){
                    $result = ['status'=>0,'message'=>trans('auth.user_not_found')];
          }
          
        return response()->json($result);   
       
    }
    
        /** send email
     * @param mixed array
     */ 
    private function sendMail($data){
        $type = $data['type'];
        $email = $data['email'];
        Mail::send('emails.'.$type, $data, function ($message) use ($email,$type) {
          $message->to("$email")->subject(trans('auth.subject_'.$type));
        });

      return true;
    }
}