<?php

namespace App\Http\Controllers;

use Auth;
use View;
use DB;
use Datatables;
use Validator;
use Image;
use App\Http\Helpers\Common_helper;
use App\Models\Categories_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Redirector;

class Categories extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check()){
            return redirect::to('auth/login');
        }
        $index_title = 'Category';
       
        return view('categories/cat_listing', ['index_title' => $index_title]);
    }

     public function list_emp()
    {
         
    
        $employees = Categories_model::select(['category_name','created_at','id']);
        return Datatables::of($employees)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_employee(Request $request)
    {
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $data = Input ::all();

        $rules = array(
            'category_name' => 'required|max:255|unique:categories,category_name,'    
        );

        $validator = Validator::make($data, $rules);
        $errors = $validator->getMessageBag()->toArray();
        if($validator->fails()){
            $msg ='';
            foreach($errors as $key=>$value){
               foreach($value as $k=>$v){
                  $msg .=$v.'<br/>'; 
                }
            }  
            return response()->json(array('status'=>0, 'message'=>$msg));  
        }

        $add_employee = Categories_model::create([
            'category_name' => $data['category_name'],
            'category_image' => $data['category_image']
        ]); 
        if($add_employee){
            $result["status"] = 1;
            $result["message"] = trans('auth.category_created');
        }else
        {
            $result["status"] = 0;
            $result["message"] =trans('auth.error_occured');
        }
        echo json_encode($result);
    }

    public function get_details(Request $request, $id)
    {  
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $user = DB::table('categories')->select('categories.*')
                                   ->where('categories.id',$id)
                                   ->get();
        $url = asset('category_images/'.$user[0]->category_image);   
        $url_check = Common_helper::URL_exists($url);  

        return view('categories.view_category', ['user' => $user[0], 'url_check' => $url_check]);
       
    }

    public function delete(Request $request, $id){
        if(!$request->ajax()){
            return trans('auth.not_ajax');
        }

        $deleted = Categories_model::where('id',$id)->delete();
               
        return  response()->json(array('status'=>1,'message'=>trans('auth.category_deleted')));
        
    }

    public function edit_view(Request $request, $id)
    {  
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $user = DB::table('categories')->select('categories.*')
                                   ->where('categories.id',$id)
                                   ->get();
        $url = asset('category_images/'.$user[0]->category_image);   
        $url_check = Common_helper::URL_exists($url);  

        return view('categories.edit_category', ['user' => $user[0], 'url_check' => $url_check]);
       
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->ajax()){
          return trans('auth.not_ajax');  
        }
        
        $data = Input ::all();

        
        $rules = array(
            'category_name' => 'required|max:255|unique:categories,category_name,'.$id    
        );
            
        $validator = Validator::make($data, $rules);
        $errors = $validator->getMessageBag()->toArray();
        if($validator->fails()){
            $msg ='';
            foreach($errors as $key=>$value){
               foreach($value as $k=>$v){
                  $msg .=$v.'<br/>'; 
                }
            }  
            return response()->json(array('status'=>0, 'message'=>$msg));  
        }

        $update_data = array(
            'category_name' => $data['category_name'],
            'category_image' => $data['category_image']
        ); 

         $update = Categories_model::where('id', $id)
            ->update($update_data);

        if($update){
            $result["status"] = 1;
            $result["message"] = trans('auth.category_updated');
        }else
        {
            $result["status"] = 0;
            $result["message"] =trans('auth.error_occured');
        }
        echo json_encode($result);
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'image_file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',       
        ]);
        $image = $request->file('image_file');

        $input['category_image'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/category_images');

        $image->move($destinationPath, $input['category_image']);
        /*$add_image = categories_model::create([
            'category_image' => $input['category_image']
        ]);*/ 

        $data["status"] = 1;
        $data["filename"] =  $input['category_image'];
        //$data["original"] = $path;
        $data["image_path"]=asset('category_images/'.$input['category_image']);
        //unlink($output_dir.$file_name.$ext);
        echo json_encode($data);
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
