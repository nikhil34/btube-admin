@extends('layouts.inner')
@section('title', "Registration")
@section('index_title'){{ $index_title }} @endSection
@section('content')
<style>
table.dataTable tr.odd { background-color:  #F4F6F6; }
</style>
            <div class="content">
                <div class="container-fluid">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                  <div class="row">
                      <div class="col-md-12">
                          <div class="card">
                              <div class="card-header card-header-icon" data-background-color="purple">
                                 <i class="material-icons">assignment</i>
                              </div>
                              
                           
                            <div class="card-content">
                                <h4 class="card-title">Registration</h4>
                                <div class="toolbar"></div>
                                  <table id='empTable' class="table table-hover">
                                    <thead>
                                      <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone No.</th>
                                        <th>Total Videos</th>
                                        <th>Join Date</th>
                                        <th>Actions</th>
                                      </tr>
                                    </thead>
                                  </table>
                              </div>
                            </div>
                          </div>
                    </div>
                </div>
              </div>



<!--Add Employee Modal -->
<div class="modal fade" id="addRegistration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Registration</h4>
      </div>
      <form name="add_registration_form" id="add_registration_form" method="post" enctype="multipart/form-data" files="true">

        <div class="modal-body" style="overflow: auto;">
          <div class="card-content">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group label-floating">
                  <label class="control-label">Category Name</label>
                  <?php echo Form::text('category_name', null, array('class'=>'form-control', 'id'=>'category_name')); ?>
                </div>
              </div>
            </div>
            <div class="row">
           <div class="col-lg-6">
                       
              <div id="upload_file_coin_type" style="float: left;">
                <div class="fileinput-new thumbnail pic-thumb">
                  <img src="{{ asset('images/placeholder_cover(200x150).png') }}" alt="" id="placeholder_image">
                </div> 
                       
                      </div>
                        <a href="#" class="btn btn-primary btn-sm" id="upload_image" style="margin-left: 15px;" ><span class="fa fa-plus " style="margin-right: 5px;"></span>Upload</a>
                       <img src="{{asset('images/loading.gif') }}" id="loader_image" style="margin-top:4px;margin-left:10px; display: none;">
                       <div class="clearfix"></div>
                       <div id="upload_file_res"></div>
                       </div>
            </div>
          </div>

                                      
                          
         </div>
      <div class="modal-footer">
      
      <div class="clearfix"></div>
      <div id="alert_area_add"></div>
         <button type="submit" class="btn btn-primary pull-right" style="margin-left: 5px;">Save Changes</button>
         <button type="button" class="btn btn-default pull-left" data-dismiss="modal"  style="margin-left: 5px;">Back</button>
         <div class="clearfix"></div>
         
      </div>
</form> 
<form name="attachment" id="attachment" enctype = 'multipart/form-data' action="{{ url('categories/upload')}}" method="post" accept-charset="utf-8">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="file" name="image_file" value="" id="image_file_1" style="display: none;">
<input  type="hidden" name="path" value="category_images"/> 
</form>
  <div class="col-xs-12">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
        @endif
    </div> 
    
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--View Modal -->
<div class="modal fade" id="view_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Category Details</h4>
      </div>
      <div class="modal-body">
       <div id="response_view"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!--Edit Modal -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Category Details</h4>
      </div>
      <div class="modal-body">
       <div id="edit_response_view"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript" src="{{asset('js/jquery.datatables.js') }}"></script>
<script type="text/javascript" src="{{asset('js/datatables.bootstrap.js')}}"></script>
<!-- jquery.form.js -->
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script>
    var emp_datatables = "";
    var placeholder_image = "{{asset('images/placeholder_cover(200x150).png') }}";
    $(document).ready(function(){
         emp_datatables = $('table#empTable').dataTable({
         processing: true,
         serverSide: true,
         ajax:{
            "dataType": 'json',
            "type": "POST",
            "url": '{!! url('registration/list') !!}'
         } ,

            "createdRow": function ( row, data, index ) {
            var links="";
            links += '<a href="#" data-employee_id="'+data[4]+'" title="View employee Details" class="btn btn-info view_employee" style="margin-right:5px;" ><i class="material-icons">search</i></a>';  
            links += '<a href="{{url('registration/edit-registration')}}/'+data[4]+'" data-employee_id="'+data[4]+'" title="Edit Details" class="btn btn-success edit_employee" style="margin-right:5px;" ><i class="material-icons">edit</i></a>';  
            links +='<a href="#" data-employee_id="'+data[4]+'" title="Delete employee"class="btn btn-danger delete_employee" style="margin-right:5px;"><i class="material-icons">delete</i></a>';  
           

            var dateSplit = data[3].split("-");            
            day = dateSplit[2].split(' ');
            var curr_date = day[0];
            var curr_month = dateSplit[1]; //Months are zero based
            var curr_year = dateSplit[0];
            
            if(data[0]==null){
              data[0] = '';
            }
            var fullname = data[0]+" "+data[5];
            
            $('td:eq(0)', row).html(fullname);
            $('td:eq(5)', row).html( links);
            $('td:eq(4)', row).html(curr_month + "-" + curr_date + "-" + curr_year );
            $('td:eq(3)', row).html('0');
        },
        aoColumnDefs: [
       
          {
             bSortable: false,
             aTargets: [5]
          },
        
          
          {
             bSearchable: false,
             aTargets: [5]
          }
        ],
        });

        //Add Employee
      $('#add_employee_form').submit(function(e){
        e.preventDefault();
            
        var values = $("#add_employee_form").serialize();
        alert(values);
        $.ajax({
           type:'post',
           url:"{!! url('categories/add') !!}",
           dataType: 'json',
           data:values,
           success:function(result)
           {
                if(result.status==1)
            {
                $('#alert_area_add').empty().html('<div class="alert alert-success  alert-dismissable" id="disappear"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
                setTimeout(function(){$('#disappear').fadeOut('slow')},3000);
                $('#add_employee_form')[0].reset();
                $('#upload_file_coin_type').empty();
                $('#upload_file_coin_type').append('<div class="fileinput-new thumbnail pic-thumb"><img src="'+placeholder_image+'" alt=""></div>');

                emp_datatables.fnDraw();
            }
            else
            {
                $('#alert_area_add').empty().html('<div class="alert alert-danger  alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+result.message+'</div>');
            }
           } 
        });
     }); 


      //View employee
 $(document).on('click','.view_employee',function(e){
    e.preventDefault();
    var id = $(this).attr('data-employee_id');
    $.ajax({
       url:"{!! url('categories/view') !!}/"+id,
       dataType: 'html',
       success:function(result)
       {
        $('#response_view').html(result);
       } 
    });
    $('#view_modal').modal('show');
 });

//Edit employee
 /*$(document).on('click','.edit_employee',function(e){
    e.preventDefault();
    var id = $(this).attr('data-employee_id');
    $.ajax({
       url:"{!! url('categories/edit') !!}/"+id,
       dataType: 'html',
       success:function(result)
       {
        $('#edit_response_view').html(result);
       } 
    });
    $('#edit_modal').modal('show');
 });*/


 //delete employee
$(document).on('click','.delete_employee',function(e){
    
    e.preventDefault();
    var response = confirm('Are you sure want to delete this employee?');
    if(response){
     var id = $(this).attr('data-employee_id');
                 $.ajax({
                         url:"{{ url('categories/delete')}}/"+id,
                         type: 'POST',
                         dataType: 'json',
                            error:function(){
                                alert("Unable to process the request");
                            },
                         success:function(result){
                               if(result.status=="1")
                               {  
                                $('#alert_area').html('<div class="alert alert-success" id="disappear_del"  >'+result.message+'</div>'); 
                                setTimeout(function(){$('#disappear_del').fadeOut('slow')},3000)
                                emp_datatables.fnDraw();
                               }
                               else
                               { 
                                 $('#alert_area').html('<div class="alert alert-danger" id="disappear_del"  ><button class="close" data-dismiss="alert">&times;</button>'+result.message+'</div>'); 
                                
                               }
                           }
                         });
       return false;
     }
  });   


   //upload image code 
 $('#upload_image').click(function(e){
       e.preventDefault();
       $('#image_file_1').trigger('click'); 
    });
    
    $('#image_file_1').change(function(){
         $("#attachment").submit();
     });
     
    var options = { 
    beforeSend: function() 
    {
        $("#loader_image").show();
        
    },
    complete: function(response) 
    {

           $("#loader_image").hide();
           var result = jQuery.parseJSON(response.responseText);
           if(result.status==0)
           {
               $('#add_coin_type_response').empty().html('<div class="alert alert-danger  alert-dismissible"  style="margin-top:5px;" ><a style="margin-top:3px;" href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+result.message+'</div>');
           }
           else if(result.status==1)
           {
               $('#upload_file_coin_type').empty();
               $('#add_coin_type_response').empty();
               $('#upload_file_coin_type').append('<div class="fileinput-new thumbnail pic-thumb" style="width: 210px; height: 160px; padding:5px"><a href="#" id="remove_image" class="remove_image" title="Remove">&times;</a><img src="'+result.image_path+'"style="width: 200px; height: 150px; alt=""><input type="hidden" value="'+result.filename+'" name="category_image" /></div>');
               
           }
    },
    error: function()
    {
        alert(" ERROR: unable to upload files");
 
    }
 
  }; 
  $("#attachment").ajaxForm(options);

 

    });
$(document).on('click','#remove_image',function(e){
       e.preventDefault();
       $('#image_file_1').val('');
       $('#upload_file_coin_type').empty();
       $('#upload_file_coin_type').append('<div class="fileinput-new thumbnail pic-thumb" ><img src="{{ asset('images/placeholder_cover(200x150).png') }}" alt=""></div>');
});
 $.ajaxSetup({
    headers: { 'X-CSRF-Token' : '{!! csrf_token() !!}' }
    });
</script>

@endSection