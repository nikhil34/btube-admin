<?php
if($user){
?>
    <table class="table table-striped table-bordered  table-condensed text-left">

        <tr>
            <td><strong>Title</strong></td>
            <td> {{ $user->title }}</td>
        </tr>
        <?php if($user->image !="" || $user->image_url != ""){ ?>
        <tr>
            <td><strong>Advertise Image</strong></td>
            <?php
                if($url_check==1){
                $url = asset('advertise_images/'.$user->image);
                    $src = $url;
                 }else{
                    $src = asset('images/placeholder_cover(200x150).png');
                }
                             
            ?>
            <td> <img src="{{ $src }}" alt="Category image" class="img-responsive category-preview-img img-width"> </td>
        </tr>
        <tr>
            <td><strong>URL</strong></td>
            <td>{{ $user->image_url }}</td>
        </tr>
        <?php } if($user->iframe_code !=""){  ?>
        <tr>
            <td><strong>Iframe Code</strong></td>
            <td>{{ $user->iframe_code }}</td>
        </tr>
        <?php } ?> 
        <tr>
            <td><strong>Position</strong></td>
            <td><?php if($user->position==1){ echo "Sidebar"; } if($user->position==2){ echo "Header"; } if($user->position==3) { echo "Content"; } if($user->position==4) { echo "Footer"; } ?></td>
        </tr>
        <tr>
            <td><strong>Created</strong></td>
            <td>{{ date('d-m-Y', strtotime($user->created_at)) }}</td>
        </tr>
    </table>
    
<?php
}
?>    
    
